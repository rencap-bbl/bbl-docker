import {useState} from "react";

export const useContacts = () => {
    const [contacts, setContacts] = useState([])
    const fetchContacts = async () => {
        const response = await fetch('/api/contacts')
        const data = await response.json()
        setContacts(data)
    }
    const reset = () => setContacts([])

    return {fetchContacts, contacts, reset}
}
