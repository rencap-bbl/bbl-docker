import Contacts from "./components/contacts/Contacts.jsx";

const App = () => (
    <div className={'m-8'}>
        <Contacts/>
    </div>
);

export default App
