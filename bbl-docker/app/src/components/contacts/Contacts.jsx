import {useContacts} from "../../hooks/useContacts.js";
import Contact from "./Contact.jsx";
import {map} from "ramda";
import {isNotNilOrEmpty} from "ramda-adjunct";

const Contacts = () => {
    const {fetchContacts, contacts, reset} = useContacts();

    return (
        <div>
            <div className={'flex flex-row, gap-4'}>
                <button className={'border rounded bg-blue-100 p-2 w-64'} onClick={fetchContacts}>
                    {'Récupérer les contacts'}
                </button>
                {isNotNilOrEmpty(contacts) &&
                    <button className={'border rounded bg-red-200 p-2 w-64'} onClick={reset}>
                        {'Reset'}
                    </button>
                }
            </div>
            <div>
                {map(contact => <Contact key={contact.id} contact={contact}/>, contacts)}
            </div>
        </div>
    );
};

export default Contacts
