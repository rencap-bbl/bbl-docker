import PropTypes from "prop-types";
import {propOr} from "ramda";

const Contact = ({contact}) => (
    <div>
        {'>'} {propOr('N/A', 'name', contact)} : {propOr('N/A', 'email', contact)}
    </div>
)

Contact.propTypes = {
    contact: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
    }).isRequired,
}

export default Contact
