package main

import (
	"bbl-docker/server/endpoints"
	"embed"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"io/fs"
	"log"
	"net/http"
	"os"
)

//go:embed dist
var content embed.FS

func main() {
	_ = godotenv.Load()

	e := echo.New()

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time=${time_rfc3339}, method=${method}, uri=${uri}, status=${status}\n",
	}))

	api := e.Group("/api")
	{
		api.GET("/contacts", endpoints.GetContacts)
	}

	var contentHandler = echo.WrapHandler(http.FileServer(getFileSystem()))
	e.GET("/*", contentHandler)

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}

func getFileSystem() http.FileSystem {
	fsys, err := fs.Sub(content, "dist")
	if err != nil {
		log.Fatal(err)
	}
	return http.FS(fsys)
}
