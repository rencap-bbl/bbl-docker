package controllers

import (
	"bbl-docker/server/dto"
	"encoding/json"
	"net/http"
	"os"
)

func GetContacts() (*[]dto.Contact, error) {
	response, err := http.Get(os.Getenv("SERVICE_URL") + "/api/contacts")
	if err != nil {
		return &[]dto.Contact{}, err
	}

	contacts := new([]dto.Contact)
	err = json.NewDecoder(response.Body).Decode(contacts)
	if err != nil {
		return &[]dto.Contact{}, err
	}
	return contacts, nil
}
