package db

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"sync"
)

var _dbHandler *mongo.Database
var once sync.Once

func Db() *mongo.Database {
	once.Do(func() {
		mongoDbUrl := os.Getenv("MONGO_DB_URL")
		dbName := os.Getenv("MONGO_DATABASE")

		fmt.Println("connection to ", mongoDbUrl+"/"+dbName+"...")
		clientOptions := options.Client().ApplyURI(mongoDbUrl)
		client, err := mongo.Connect(context.TODO(), clientOptions)
		if err != nil {
			log.Fatal(err)
		}

		err = client.Ping(context.Background(), nil)

		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("connected")

		_dbHandler = client.Database(dbName)
	})
	return _dbHandler
}

func ContactsHandler() *mongo.Collection {
	return Db().Collection("contacts")
}
