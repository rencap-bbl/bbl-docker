package services

import (
	"bbl-docker-service/db"
	"bbl-docker-service/models"
	"context"
	"go.mongodb.org/mongo-driver/bson"
)

func GetContacts() (*[]models.Contact, error) {
	cursor, err := db.ContactsHandler().Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}

	results := new([]models.Contact)
	err = cursor.All(context.TODO(), results)
	if err != nil {
		return nil, err
	}

	return results, nil
}
