package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Contact struct {
	Id    primitive.ObjectID `bson:"_id,omitempty"`
	Name  string             `bson:"name,omitempty"`
	Email string             `bson:"email,omitempty"`
}
