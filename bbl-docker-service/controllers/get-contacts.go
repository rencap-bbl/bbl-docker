package controllers

import (
	"bbl-docker-service/dto"
	"bbl-docker-service/services"
)

func GetContacts() (*[]dto.Contact, error) {
	contacts, err := services.GetContacts()
	if err != nil {
		return nil, err
	}

	contactsResponse := make([]dto.Contact, len(*contacts))
	for i, contact := range *contacts {
		contactsResponse[i] = dto.Contact{
			Id:    contact.Id.Hex(),
			Name:  contact.Name,
			Email: contact.Email,
		}
	}

	return &contactsResponse, nil
}
