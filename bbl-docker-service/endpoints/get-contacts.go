package endpoints

import (
	"bbl-docker-service/controllers"
	"github.com/labstack/echo/v4"
	"net/http"
)

func GetContacts(c echo.Context) error {
	contacts, err := controllers.GetContacts()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]any{"error": true})
	}
	return c.JSON(http.StatusOK, *contacts)
}
