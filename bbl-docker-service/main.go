package main

import (
	"bbl-docker-service/db"
	"bbl-docker-service/endpoints"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"os"
)

func main() {
	_ = godotenv.Load()

	db.Db()

	e := echo.New()

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "time=${time_rfc3339}, method=${method}, uri=${uri}, status=${status}\n",
	}))

	api := e.Group("/api")
	{
		api.GET("/contacts", endpoints.GetContacts)
	}

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
